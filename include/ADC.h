#ifndef ADC_H
#define ADC_H

#include "ps2000.h"

#include "Vector.h"

const short ADC_TRUE = 1;
const short ADC_FALSE = 0;

struct CHANNEL_CONFIG
{
	short CHANNEL;
	short ENABLED;
	short DC;
	short RANGE;
};

struct TRIGGER_CONFIG
{
	short SOURCE;
	short THRESHOLD;
	short DIRECTION;
	short DELAY;
	short AUTO_TRIGGER_MS;
};

struct MEASURE_CONFIG
{
	int NUMBER_SAMPLES;
	int TIME_BASE;
	short OVERSAMPLE;
};

class ADC
{
public:
	ADC();
	~ADC();

	void open();
	void close();
	
	/// We use Channel A as Data and Channel B as Trigger
	void config();
	
	Vector<float> get_measure();
protected:
	const double MAX_MEASURE_DELAY_SEC = 20.0;

	short m_Handle;
	
	float m_Voltage_Range;
	
	int m_Number_Samples;
	short m_Time_Base;
	short m_Oversample;
	
	void config_oscilloscope(CHANNEL_CONFIG CH_A, CHANNEL_CONFIG CH_B, TRIGGER_CONFIG TRIGGER);
	void config_measure(MEASURE_CONFIG MEASURE);
private:

};

#endif // ADC_H
