#ifndef ACQUISITION_H
#define ACQUISITION_H

#include "Structures.h"

#include "Motor_Controller.h"
#include "ADC.h"

#include "Buffer.h"

class Acquisition
{
public:
	Acquisition();
	~Acquisition();
	
	void set_stop_flag(bool flag);
	
	void set_buffer(Buffer<RADiCal_Scanning<float> >* p_Buffer);
	void set_Motor(Motor_Controller* p_Motor);
	void set_ADC(ADC* p_ADC);
	
	void get_measure();
	
	void run();
protected:
	Buffer<RADiCal_Scanning<float> >* m_Scanning_Buffer;
	Motor_Controller* m_Motor;
	ADC* m_ADC;
	
	std::atomic<bool> f_STOP;
private:

};

//	Free functions
void Acquisition_Task(Acquisition* p_Acquisition);

#endif //ACQUISITION_H
