#ifndef RADICAL_CONST_H
#define RADICAL_CONST_H

const int RADiCal_Number_Presamples = 13000;
const int RADiCal_Number_Samples = 65536;
const float RADiCal_Sampling_Frecuency_Hz = 312500;
const int RADiCal_Sampling_Frecuency_Code = 6;
const int RADiCal_Oversampling = 0;

const int RADiCal_Interval_Length = RADiCal_Number_Samples;
const int RADiCal_Number_Intervals = RADiCal_Number_Samples/RADiCal_Interval_Length;
const float RADiCal_Decision_Margin = 10.0;

//	Post-processing

const float RADiCal_Light_Velocity = 3e8;
const float RADiCal_Delta_F_Hz = 150e6;
const float RADiCal_Tm_s = 84e-3;

#endif //RADICAL_CONST_H
