#ifndef MOTOR_CONTROLER_H
#define MOTOR_CONTROLER_H

#include <string>

#include <QString>
#include <QStringList>

#include <atomic>

#include "Structures.h"

#include "qextserialport.h"

struct Motor
{
    float pos;
    QextSerialPort* port;        

    bool LIM_SW_P   = false;
    bool LIM_SW_N   = false;
    bool ANY_ERROR  = false;
    bool MOTOR_RUNS = false;
};


//vertical range steps: 0 - 16000 : 0 - 90º
//horizontal range steps: 0 - 32000 : 0 - 180º
class Motor_Controller
{
public:
    Motor_Controller();
    ~Motor_Controller();

    void open_ports();
    void close_ports();
    void init_motors();
    void reset_motor_initiators(Motor& m);
    std::string create_frame(std::string data);

    void read_ext_status(); //para comprobar si ha terminado el reset

    void set_motor_position(Motor& m, float deg, bool wait);
    QString read_ans(Motor& m);
    QString send_command(Motor& m, std::string cmd);
    void update_motor_status(Motor& m, QString ans);
    
    //void start_scan(float scan_v, float scan_h);
    
    void set_limits(float limit_h, float limit_v);
	void increment();
	
	void test();
	
	Pointing<float> get_pointing();
	
    //Hay que utilizar 2 puertos serie para la conexion, cada uno tendra la misma direccion ADDR = 0
    //Tal vez haya que variar el nombre de los puertos más adelante
protected:
	Motor motor_h;
    Motor motor_v;

    const QString PORT_H_NAME = "ttyUSB0";
    const QString PORT_V_NAME = "ttyUSB1";
    
    const char ADDR = '0';
    
    const float BW = 2;
    const float DEG2STEP = 16000/90;
    const int SL_INTERVAL_MS = 10; //sleep interval in ms
    
    const std::string SCAN_SLOW = "50";
    const std::string SCAN_FAST = "100";
    
    float scan_limit_h;
    float scan_limit_v;
    
    float increment_step;
    
	std::atomic<bool> f_STOP;
    
    //Tienen que ser accesibles desde fuera para poder
    //indicar que motor mover

private:

};

#endif // MOTOR_CONTROLER_H
