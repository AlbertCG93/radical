#ifndef PROCESSING_H
#define PROCESSING_H

#include "Structures.h"

#include "Buffer.h"

#include "Utility.h"

#include <atomic>

#include <mutex>
#include <condition_variable>

#include <thread>

class Processing
{
public:
	Processing();
	~Processing();
	
	void set_stop_flag(bool flag);
	
	void set_scanning_buffer(Buffer<RADiCal_Scanning<float> >* p_Buffer);
	bool scanning_buffer_empty();
	
	void set_processing_buffer(Buffer<RADiCal_Detected_Peak<float> >* p_Buffer);
	bool processing_buffer_empty();
	
	void set_utility(Utility<float>* p_Utility);

	void process();
	
	void notify_SB();
	void notify_PB();
	
	void run();
protected:
	Buffer<RADiCal_Scanning<float> >* m_Scanning_Buffer;
	Buffer<RADiCal_Detected_Peak<float> >* m_Processing_Buffer;
	
	Utility<float>* m_Utility;
	
	std::condition_variable* m_SB_cv;
	std::condition_variable* m_PB_cv;
	
	std::atomic<bool> f_STOP;
private:

};

//	Free functions
void Processing_Task(Processing* p_Processing);

#endif // PROCESSING_H
