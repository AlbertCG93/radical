#ifndef UTILITY_H
#define UTILITY_H

#include "Structures.h"

#include "Vector.h"

#include <fstream>

#include <string>

#include <cmath> 
#include <complex>

template <class T>
class Utility
{
public:
    const std::complex<T> I = std::complex<T>(0, 1);
    const T PI = 4.0 * atan(1.0);
    
    Utility();
    ~Utility();
    
    void add_zeros(Vector<T>& vector, int size);
    
    void initialize_exponential_factors(int n);
    
    Vector<std::complex<T> > rad2_ct_fft(Vector<std::complex<T> >& Vector_X, int n);
    Vector<std::complex<T> > rad2_ct_fft(Vector<T>& Vector_X, int n);
    Vector<std::complex<T> > rad2_ct_fft_old(Vector<std::complex<T> >& Vector_X, int n);
    
    Vector<T> vector_abs(Vector<std::complex<T> >& Vector_X);
    
    std::vector<Detected_Peak<T> > peak_det(Vector<T>& X);
    
    Vector<std::complex<T> > read_file_complex(std::string filename);
    Vector<T> read_file_real(std::string filename);
    
    void write_file_complex(std::string filename, Vector<std::complex<T> > X_Vector, int precision);
    void write_file_real(std::string filename, Vector<T> X_Vector, int precision);
protected:
    std::vector<std::vector<std::complex<T> > > Exponential_Factors_K_M;
    std::vector<std::complex<T> >  Exponential_Factors_K;
private:

};

#endif // UTILITY_H
