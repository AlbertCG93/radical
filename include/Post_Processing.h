#ifndef POST_PROCESSING_H
#define POST_PROCESSING_H

#include "Structures.h"

#include "Buffer.h"

#include <atomic>

#include <mutex>
#include <condition_variable>

#include <thread>

class Post_Processing
{
public:
	Post_Processing();
	~Post_Processing();
	
	void set_stop_flag(bool flag);
		
	void set_processing_buffer(Buffer<RADiCal_Detected_Peak<float> >* p_Buffer);
	bool processing_buffer_empty();
	
	void set_display_buffer(Buffer<RADiCal_Objective<float> >* p_Buffer);
	bool display_buffer_empty();
	
	void process();
	
	void notify_PB();
	void notify_DB();
	
	void run();
protected:
	Buffer<RADiCal_Detected_Peak<float> >* m_Processing_Buffer;
	Buffer<RADiCal_Objective<float> >* m_Display_Buffer;

	std::condition_variable* m_PB_cv;
	std::condition_variable* m_DB_cv;
	
	std::atomic<bool> f_STOP;
private:

};

//	Free functions
void Post_Processing_Task(Post_Processing* p_Post_Processing);

#endif // POST_PROCESSING_H
