/********************************************************************************
** Form generated from reading UI file 'Main_Window.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAIN_WINDOW_H
#define UI_MAIN_WINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Main_Window
{
public:
    QAction *actionConnection;
    QWidget *centralWidget;
    QWidget *widget;
    QTextEdit *textEdit;
    QListWidget *targetList;
    QLineEdit *azimuthEdit;
    QLineEdit *distanceEdit;
    QLineEdit *heightEdit;
    QLineEdit *radarcrossEdit;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Main_Window)
    {
        if (Main_Window->objectName().isEmpty())
            Main_Window->setObjectName(QString::fromUtf8("Main_Window"));
        Main_Window->resize(600, 600);
        Main_Window->setMinimumSize(QSize(600, 600));
        actionConnection = new QAction(Main_Window);
        actionConnection->setObjectName(QString::fromUtf8("actionConnection"));
        centralWidget = new QWidget(Main_Window);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(10, 10, 400, 400));
        widget->setMinimumSize(QSize(400, 400));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(10, 410, 400, 170));
        textEdit->setMinimumSize(QSize(400, 170));
        targetList = new QListWidget(centralWidget);
        targetList->setObjectName(QString::fromUtf8("targetList"));
        targetList->setGeometry(QRect(420, 10, 170, 185));
        azimuthEdit = new QLineEdit(centralWidget);
        azimuthEdit->setObjectName(QString::fromUtf8("azimuthEdit"));
        azimuthEdit->setEnabled(true);
        azimuthEdit->setGeometry(QRect(515, 205, 75, 16));
        distanceEdit = new QLineEdit(centralWidget);
        distanceEdit->setObjectName(QString::fromUtf8("distanceEdit"));
        distanceEdit->setGeometry(QRect(515, 230, 75, 16));
        heightEdit = new QLineEdit(centralWidget);
        heightEdit->setObjectName(QString::fromUtf8("heightEdit"));
        heightEdit->setGeometry(QRect(515, 255, 75, 16));
        radarcrossEdit = new QLineEdit(centralWidget);
        radarcrossEdit->setObjectName(QString::fromUtf8("radarcrossEdit"));
        radarcrossEdit->setGeometry(QRect(515, 280, 75, 16));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(420, 205, 90, 16));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(420, 230, 90, 16));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(420, 255, 90, 16));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(420, 280, 90, 16));
        Main_Window->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(Main_Window);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Main_Window->setStatusBar(statusBar);

        retranslateUi(Main_Window);

        QMetaObject::connectSlotsByName(Main_Window);
    } // setupUi

    void retranslateUi(QMainWindow *Main_Window)
    {
        Main_Window->setWindowTitle(QApplication::translate("Main_Window", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionConnection->setText(QApplication::translate("Main_Window", "Motor connection", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Main_Window", "Azimuth (\302\272):", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Main_Window", "Distance (m):", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Main_Window", "Height (m):", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Main_Window", "RCS:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Main_Window: public Ui_Main_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAIN_WINDOW_H
