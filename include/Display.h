#ifndef DISPLAY_H
#define DISPLAY_H

#include "Structures.h"

#include "Buffer.h"

#include <atomic>

#include <mutex>
#include <condition_variable>

#include <thread>

class Display
{
public:
	Display();
	~Display();
	
	void set_stop_flag(bool flag);
	
	void set_display_buffer(Buffer<RADiCal_Objective<float> >* p_Buffer);
	bool display_buffer_empty();
	
	void display();
	
	void notify_DB();
	
	void run();
protected:
	Buffer<RADiCal_Objective<float> >* m_Display_Buffer;
	
	std::condition_variable* m_DB_cv;
	
	std::atomic<bool> f_STOP;
private:

};

//	Free functions
void Display_Task(Display* p_Display);

#endif //DISPLAY_H
