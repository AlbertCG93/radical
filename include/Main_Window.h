#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "Structures.h"

#include "Buffer.h"

#include <atomic>

#include <mutex>
#include <condition_variable>

#include <thread>

#include <QApplication>
#include <QMainWindow>
//#include <QCloseEvent>

#include "qwt_polar_plot.h"
#include "qwt_polar_grid.h"
#include "qwt_polar_marker.h"
#include "QListWidgetItem"
#include "qwt_symbol.h"

struct target
{
    RADiCal_Objective<float> ro;
    QwtPolarMarker* marker;
};

namespace Ui {
class Main_Window;
}

class Main_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Main_Window(QWidget *parent = 0);
    ~Main_Window();
	
	void set_display_buffer(Buffer<RADiCal_Objective<float> >* p_Buffer);
	
	bool display_buffer_empty();

	void set_shutdown(void (*f)());
	void closeEvent(QCloseEvent *event);
	
	void send_message(std::string msg);
protected:
    int prevIndex;
    QVector<target> targets;
    
    QwtPolarPlot* polar_plot;
    QwtPolarGrid* d_grid;
    
    Buffer<RADiCal_Objective<float> >* m_Display_Buffer;
	
	std::condition_variable* m_DB_cv;
	
	std::atomic<bool> f_STOP;
	
	void (*shutdown)();
private slots:
    void on_targetList_currentRowChanged(int currentRow);
    
public slots:
	void displayTarget(QString);

private:
    Ui::Main_Window *ui;
    int PLOT_RADIUS = 500;

};

#endif // MAIN_WINDOW_H
