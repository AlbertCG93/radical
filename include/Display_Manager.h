#ifndef DISPLAY_H
#define DISPLAY_H

#include "Structures.h"

#include "Buffer.h"

#include <atomic>
#include "Main_Window.h"

#include <mutex>
#include <condition_variable>

#include <thread>

class Display_Manager : public QObject
{
	  Q_OBJECT  
	  
public:
	Main_Window* mw;
	
	Display_Manager();
	~Display_Manager();
	
	void set_stop_flag(bool flag);
	
	void set_display_buffer(Buffer<RADiCal_Objective<float> >* p_Buffer);
	bool display_buffer_empty();
	
	void display();
	
	void notify_DB();

	void run();
protected:
	Buffer<RADiCal_Objective<float> >* m_Display_Buffer;
	
	std::condition_variable* m_DB_cv;
	
	std::atomic<bool> f_STOP;
signals:
    void newTarget(QString);	

};

//	Free functions
void Display_Manager_Task(Display_Manager* p_Display);

#endif //DISPLAY_H
