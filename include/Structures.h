#ifndef STRUCTURES_H
#define STRUCTURES_H

#include "Vector.h"

template <class T>
struct Pointing
{
	T Azimuth;
	T Elevation;
};

template <class T>
struct RADiCal_Scanning
{
	Pointing<T> s_Pointing;
	Vector<T> s_Scanning;
};

template <class T>
struct Detected_Peak
{
	T Amplitude;
	T Frecuency;
};

template <class T>
struct RADiCal_Detected_Peak
{
	Pointing<T> s_Pointing;
	Detected_Peak<T> s_Detected_Peak;
};

template <class T>
struct RADiCal_Objective
{
	T Horitzontal_Distance;
	T Azimuth;
	
	T Height;
	
	T RCS;
	
	/*	Explanation:
	 * 		"isTARGET = true" means a moving object
	 * 		"isTARGET = false" means a non-moving object (cluster)
	 */
	bool isTARGET;
};

template struct Pointing<float>;

template struct RADiCal_Scanning<float>;

template struct Detected_Peak<float>;
template struct RADiCal_Detected_Peak<float>;

template struct RADiCal_Objective<float>;

#endif //STRUCTURES_H
