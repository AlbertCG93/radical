#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

#include <vector>

template <class T>
class Vector
{
public:
	Vector();
	Vector(int dimension);
	~Vector();

	Vector<T>& operator+=(Vector<T>& Vector_B);
    Vector<T>& operator-=(Vector<T>& Vector_B);
    T& operator[](int index);

	void push_back(T value);

	int dimension(void);
protected:
	std::vector<T> m_Vector;
private:

};

// Free Functions

template <class T>
Vector<T> operator+(Vector<T>& Vector_A, Vector<T>& Vector_B);

template <class T>
Vector<T> operator-(Vector<T>& Vector_A, Vector<T>& Vector_B);

template <class T>
Vector<T> operator*(T Scalar, Vector<T>& Vector_A);

template <class T>
std::ostream& operator<<(std::ostream& out, Vector<T> Vector_A);

#endif // VECTOR_H
