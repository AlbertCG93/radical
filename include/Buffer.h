#ifndef BUFFER_H
#define BUFFER_H

#include "Structures.h"

#include <queue>

#include <mutex>
#include <condition_variable>

template <class T>
class Buffer
{
public:
	Buffer();
	~Buffer();
	
	std::condition_variable* get_condition_variable();

	bool empty();

	void update(T element);
	T retrieve();
protected:
	std::queue<T> m_Buffer;
	
	std::mutex m_mtx;
	std::condition_variable m_cv;
private:

};

#endif // SCANNING_BUFFER_H
