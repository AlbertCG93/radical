#include "Display_Manager.h"

#include "RADiCal_Const.h"
#include "Main_Window.h"

#include <chrono>

#include <thread>
#include <QThread>
#include <QTimer>
#include <QObject>

Display_Manager::Display_Manager()
{
	 set_stop_flag(false);
}

Display_Manager::~Display_Manager()
{

}

void Display_Manager::set_stop_flag(bool flag)
{
	f_STOP.store(flag, std::memory_order_relaxed);
}

void Display_Manager::set_display_buffer(Buffer<RADiCal_Objective<float> >* p_Buffer)
{
	m_Display_Buffer = p_Buffer;
	m_DB_cv = p_Buffer->get_condition_variable();
}

bool Display_Manager::display_buffer_empty()
{
	return m_Display_Buffer->empty();
}

void Display_Manager::display()
{
	std::mutex mtx;
	std::unique_lock<std::mutex> lck (mtx, std::defer_lock);
	
	while(display_buffer_empty())
	{
		if(f_STOP.load(std::memory_order_relaxed) == true)
			return;
			
		m_DB_cv->wait(lck);
	}
		
	RADiCal_Objective<float> r_Objective = m_Display_Buffer->retrieve();
		
	QString parsed_r_Objective = QString::number(r_Objective.Horitzontal_Distance)
				+ ":" + QString::number(r_Objective.Azimuth)
				+ ":" + QString::number(r_Objective.Height)
				+ ":" + QString::number(r_Objective.RCS)
				+ ":" + QString::number(r_Objective.isTARGET);
	emit newTarget(parsed_r_Objective);
}

void Display_Manager::notify_DB()
{
	m_DB_cv->notify_all();
}

void Display_Manager::run()
{
	while(not f_STOP.load(std::memory_order_relaxed))
		display();
	
	std::cerr << "Display Manager se cierra" << std::endl;
}

void Display_Manager_Task(Display_Manager* p_Display)
{
	p_Display->run();
}
