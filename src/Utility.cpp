#include "RADiCal_Const.h"

#include "Utility.h"

#include <utility>

template <class T>
Utility<T>::Utility()
{
	
}

template <class T>
Utility<T>::~Utility()
{
	
}

template <class T>
void Utility<T>::add_zeros(Vector<T>& vector, int size)
{
	while(vector.dimension() < size)
	{
		vector.push_back(0);
	}
}

template <class T>
void Utility<T>::initialize_exponential_factors(int n)
{
	Exponential_Factors_K_M = std::vector<std::vector<std::complex<T> > >(n/2, std::vector<std::complex<T> > (n/2));
	Exponential_Factors_K = std::vector<std::complex<T> >(n/2);
	for(int k = 0; k < n/2; ++k)
	{
		for(int m = 0; m < n/2; ++m)
			Exponential_Factors_K_M[k][m] = exp(-I*(T)(4)*PI/(T)(n)*(T)(m*k));
		
		Exponential_Factors_K[k] = exp(-I*(T)(2)*PI/(T)(n)*(T)(k));
	}
}

template <class T>
Vector<std::complex<T> > Utility<T>::rad2_ct_fft(Vector<std::complex<T> >& X, int n)
{
	Vector<std::complex<T> > Result = Vector<std::complex<T> >(n);
	
	Vector<std::complex<T> > X_Even = Vector<std::complex<T> >(n/2);
	Vector<std::complex<T> > X_Odd = Vector<std::complex<T> >(n/2);
	for(int i = 0; i < n/2; ++i)
	{
		X_Even[i] = X[2*i];
		X_Odd[i] = X[2*i + 1];
	}
	
	std::vector<std::complex<T> > Even = std::vector<std::complex<T> >(n/2);
	std::vector<std::complex<T> > Odd = std::vector<std::complex<T> >(n/2);

	for(int k = 0; k < n/2; ++k)
	{
		Even[k] = 0;
		for(int m = 0; m < n/2; ++m)
			Even[k] += X_Even[m]*Exponential_Factors_K_M[k][m];
		
		Odd[k] = 0;
		for(int m = 0; m < n/2; ++m)
			Odd[k] += X_Odd[m]*Exponential_Factors_K_M[k][m];
		
		Result[k] = Even[k] + Exponential_Factors_K[k]*Odd[k];
		Result[k + n/2] = Even[k] - Exponential_Factors_K[k]*Odd[k];
	}
	
	return Result;
}

template <class T>
Vector<std::complex<T> > Utility<T>::rad2_ct_fft(Vector<T>& X, int n)
{
	Vector<std::complex<T> > Result = Vector<std::complex<T> >(n);
	
	Vector<T> X_Even = Vector<T>(n/2);
	Vector<T> X_Odd = Vector<T>(n/2);
	
	for(int i = 0; i < n/2; ++i)
	{
		X_Even[i] = X[2*i];
		X_Odd[i] = X[2*i + 1];
	}
	
	std::vector<std::complex<T> > Even = std::vector<std::complex<T> >(n/2);
	std::vector<std::complex<T> > Odd = std::vector<std::complex<T> >(n/2);

	for(int k = 0; k < n/2; ++k)
	{
		Even[k] = 0;
		for(int m = 0; m < n/2; ++m)
			Even[k] += X_Even[m]*Exponential_Factors_K_M[k][m];
		
		Odd[k] = 0;
		for(int m = 0; m < n/2; ++m)
			Odd[k] += X_Odd[m]*Exponential_Factors_K_M[k][m];
		
		Result[k] = Even[k] + Exponential_Factors_K[k]*Odd[k];
		Result[k + n/2] = Even[k] - Exponential_Factors_K[k]*Odd[k];
	}
	
	return Result;
}

template<class T>
Vector<T> Utility<T>::vector_abs(Vector<std::complex<T> >& Vector_X)
{
	int n = Vector_X.dimension();
	
	Vector<T> Result = Vector<T>(n);
	
	for(int i = 0; i < n; ++i)
		Result[i] = std::abs(Vector_X[i]);
		
	return Result;
}

/*	This function is expected to seek and return the frequencial position of tones. 
 *  As the signal (X) is polluted with noise,
 *  an algorythm has to be applied to get the previously mentioned information.
 */
template <class T>
std::vector<Detected_Peak<T> > Utility<T>::peak_det(Vector<T>& X) 
{
	// Division of the received signals within intervals

	// Creation of vector (M) containing the means of each interval
	Vector<T> M = Vector<T>(RADiCal_Number_Intervals);

	T sum = 0;
	for (int j = 0; j < RADiCal_Number_Samples; ++j)
	{
		sum += X[j];
		
		if ((j + 1) % RADiCal_Interval_Length == 0) 
		{
			//Interval mean computation
			M[j/RADiCal_Interval_Length] = sum/(T)RADiCal_Interval_Length;

			sum = 0;
		}
	}
	
	//Ellimination of noise component
	    	
	Vector<T> Y = Vector<T>(RADiCal_Number_Samples);
	
	for (int j = 0; j < RADiCal_Number_Samples; ++j)
	{
	    Y[j] = X[j] - ((T)(1 + RADiCal_Decision_Margin))*M[j/RADiCal_Interval_Length];
		
	    // This causes that the noise samples take negative values.
	    // Applying the decision margin we force that to happen.
	    if (Y[j] < 0)
	    	Y[j] = 0;
	}
	
	//Algorythm for searching the Targets

	//Creation of information vectors regarding detected peaks
	std::vector<Detected_Peak<T> > R;

	//No need to use all the samples, as we are working on the frequency domain

	// Direction: '1' =  Searching for a Maximum, '0' = Searching for a Minimum
	int direction = 1; 
	T last_sample = 0;
	for (int j = 0; j < RADiCal_Number_Samples/2; ++j)
	{
        if (Y[j] < last_sample)
        {
            if (direction == 1)
			{
				Detected_Peak<T> peak;

                peak.Amplitude = last_sample + ((T)(1 + RADiCal_Decision_Margin))*M[(j - 1)/RADiCal_Interval_Length];
				peak.Frecuency = (j - 1) * RADiCal_Sampling_Frecuency_Hz/RADiCal_Number_Samples;
				
				R.push_back(peak);
			}
			
			if(Y[j] != 0)
            	direction = 0;
            else
            	direction = 1;
        }

		last_sample = Y[j];
	}
	
	if(last_sample > 0 and direction == 1)
	{
		Detected_Peak<T> peak;

        peak.Amplitude = last_sample + ((T)(1 + RADiCal_Decision_Margin))*M[(RADiCal_Number_Samples/2 - 1)/RADiCal_Interval_Length];
		peak.Frecuency = (RADiCal_Number_Samples/2 - 1) * RADiCal_Sampling_Frecuency_Hz/RADiCal_Number_Samples;
		
		R.push_back(peak);
	}

	return R;
}

/// Format of a Complex Number: ("Real_Part","Imaginary_Part")

template<class T>
Vector<std::complex<T> > Utility<T>::read_file_complex(std::string filename)
{
	std::fstream fs;
	fs.open(filename, std::fstream::in);
	
	int n;
	fs >> n;
	
	Vector<std::complex<T> > X_Vector = Vector<std::complex<T> >(n);
	
	if(fs.is_open() == false)
		throw "Couldn't open file";
	
	for(int i = 0; i < n; ++i)
	{
		char ignore;
		T real, imag;
		fs >> ignore >> real >> ignore >> imag >> ignore;
		
		X_Vector[i] = std::complex<T>(real, imag);
	}
	
	fs.close();
	
	return X_Vector;
}

template<class T>
Vector<T> Utility<T>::read_file_real(std::string filename)
{
	std::fstream fs;
	fs.open(filename, std::fstream::in);
	
	int n;
	fs >> n;
	
	Vector<T> X_Vector = Vector<T>(n);
	
	if(fs.is_open() == false)
		throw "Couldn't open file";
	
	for(int i = 0; i < n; ++i)
		fs >> X_Vector[i];

	fs.close();
	
	return X_Vector;
}

template<class T>
void Utility<T>::write_file_complex(std::string filename, Vector<std::complex<T> > X_Vector, int precision)
{
	std::fstream fs;
	fs.precision(precision);
	fs.open(filename, std::fstream::out);
	
	int n = (int)X_Vector.dimension();
	fs << n << std::endl;
	
	if(fs.is_open() == false)
		throw "Couldn't open file";
	
	for(int i = 0; i < n; ++i)
		fs << (i == 0 ? "" : " ") << "(" << std::real(X_Vector[i]) << "," << std::imag(X_Vector[i]) << ")";
	
	fs.close();
	
	return;
}

template<class T>
void Utility<T>::write_file_real(std::string filename, Vector<T> X_Vector, int precision)
{
	std::fstream fs;
	fs.precision(precision);
	fs.open(filename, std::fstream::out);
	
	int n = (int)X_Vector.dimension();
	fs << n << std::endl;
	
	if(fs.is_open() == false)
		throw "Couldn't open file";
	
	for(int i = 0; i < n; ++i)
		fs << (i == 0 ? "" : " ") << X_Vector[i];
	
	fs.close();
	
	return;
}

// Explicit Instantiations

template class Utility<double>;
template class Utility<float>;
