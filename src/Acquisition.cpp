#include "Acquisition.h"

//temporal include
#include <iostream>

Acquisition::Acquisition()
{
	
}
	
Acquisition::~Acquisition()
{
	
}

void Acquisition::set_stop_flag(bool flag)
{
	f_STOP.store(flag, std::memory_order_relaxed);
}

void Acquisition::set_buffer(Buffer<RADiCal_Scanning<float> >* p_Buffer)
{
	m_Scanning_Buffer = p_Buffer;
}
	
void Acquisition::set_Motor(Motor_Controller* p_Motor)
{
	m_Motor = p_Motor;
}
	
void Acquisition::set_ADC(ADC* p_ADC)
{
	m_ADC = p_ADC;
}

void Acquisition::get_measure()
{	
	//ADC
	Vector<float> scanning = m_ADC->get_measure();
	
	//Motor
	m_Motor->increment();
	Pointing<float> pointing = m_Motor->get_pointing();
	
	RADiCal_Scanning<float> r_scanning = {pointing, scanning};
    
	m_Scanning_Buffer->update(r_scanning);
	
	std::cerr << "Adquisition ha tomado una medida" << std::endl;
}

void Acquisition::run()
{
	while(not f_STOP.load(std::memory_order_relaxed))
		get_measure();
	
	std::cerr << "Acquisition se cierra" << std::endl;
}

void Acquisition_Task(Acquisition* p_Acquisition)
{
	p_Acquisition->run();
}
