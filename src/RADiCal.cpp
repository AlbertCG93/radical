#include "RADiCal_Const.h"
#include "Structures.h"

#include "Buffer.h"

#include "Vector.h"
#include "Utility.h"
#include "Motor_Controller.h"
#include "ADC.h"
#include "Acquisition.h"
#include "Processing.h"
#include "Post_Processing.h"
#include "Display_Manager.h"
#include "Main_Window.h"

#include <string>

#include <chrono>

#include <mutex>
#include <condition_variable>

#include <thread>

#include <iostream>

#include <QApplication>
#include <QMetaType>

Buffer<RADiCal_Scanning<float> >* p_SB;
Buffer<RADiCal_Detected_Peak<float> >* p_PB;
Buffer<RADiCal_Objective<float> >* p_DB;

Motor_Controller* p_Motor;
ADC* p_ADC;
Acquisition* p_Acquisition;
Utility<float>* p_Utility;
Processing* p_Processing;
Post_Processing* p_Post_Processing;
Display_Manager* p_Display_Manager;
Main_Window* p_Main_Window;

std::thread* t_Adquisition;
std::thread* t_Processing;
std::thread* t_Post_Processing;
std::thread* t_Display;

bool f_STOP = false;

void shutdown()
{
	std::cerr << "Shutdown called." << std::endl;
	
	f_STOP = true;
	
	p_Acquisition->set_stop_flag(true);
	
	p_Processing->set_stop_flag(true);
    p_Processing->notify_SB();
    
    p_Post_Processing->set_stop_flag(true);
    p_Post_Processing->notify_PB();
    
    p_Display_Manager->set_stop_flag(true);
	p_Display_Manager->notify_DB();
    
    t_Adquisition->join();
    t_Processing->join();
    t_Post_Processing->join();
    t_Display->join();
    
    delete p_SB;
    delete p_PB;
    delete p_DB;
    
    delete p_Motor;
    delete p_ADC;
    delete p_Acquisition;
    delete p_Utility;
    delete p_Processing;
    delete p_Post_Processing;
    delete p_Display_Manager;
    delete p_Main_Window;
     
    delete t_Adquisition;
    delete t_Processing;
    delete t_Post_Processing;
    delete t_Display;
}

int main(int argc, char *argv[])
{	
    /// Setting up the envinorment
 
	QApplication app(argc, argv);   
		
    //  Scanning Buffer set up
    p_SB = new Buffer<RADiCal_Scanning<float> >();
    
    //  Processing Buffer set up
    p_PB = new Buffer<RADiCal_Detected_Peak<float> >();
    
    //	Display Buffer set up
    p_DB = new Buffer<RADiCal_Objective<float> >();
    
    //  Motor set up
	p_Motor = new Motor_Controller();
	p_Motor->open_ports();
	p_Motor->init_motors();
	p_Motor->set_limits(0, 0); 
	   
    //  ADC set up
    p_ADC = new ADC();
    p_ADC->config();
    
    //	Acquisition set up
    p_Acquisition = new Acquisition();
    p_Acquisition->set_buffer(p_SB);
    p_Acquisition->set_ADC(p_ADC);
    p_Acquisition->set_Motor(p_Motor);
   
    //	Utility set up
    p_Utility = new Utility<float>();
    p_Utility->initialize_exponential_factors(RADiCal_Number_Samples);

    //  Processing set up
    p_Processing = new Processing();
    p_Processing->set_scanning_buffer(p_SB);
    p_Processing->set_processing_buffer(p_PB);
    p_Processing->set_utility(p_Utility);

    //  Post-Processing set up
    p_Post_Processing = new Post_Processing();
    p_Post_Processing->set_processing_buffer(p_PB);
    p_Post_Processing->set_display_buffer(p_DB);
    
    //	Display Manager set up
    p_Display_Manager = new Display_Manager();
    p_Display_Manager->set_display_buffer(p_DB); 
    
	//	Main Window set up
    p_Main_Window = new Main_Window();
	p_Main_Window->set_shutdown(shutdown);
    p_Main_Window->show();
    
    //	Display_Manager <-> Main_Window linking
	QObject::connect(p_Display_Manager, SIGNAL(newTarget(QString)), p_Main_Window, SLOT(displayTarget(QString)));
    
	// Running loop
    t_Adquisition = new std::thread(Acquisition_Task, p_Acquisition);
    t_Processing = new std::thread(Processing_Task, p_Processing);
    t_Post_Processing = new std::thread(Post_Processing_Task, p_Post_Processing);
    t_Display = new std::thread(Display_Manager_Task, p_Display_Manager);

    //RADiCal_Objective<float> r1 = {30, 15, 30, 38, true};
	//RADiCal_Objective<float> r2 = {60, 70, 60, 50, false};	
	//p_DB->update(r1);
	//p_DB->update(r2);
            
    return app.exec();
}
