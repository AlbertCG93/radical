#include "RADiCal_Const.h"

#include "Processing.h"

#include "Vector.h"

Processing::Processing()
{
	 set_stop_flag(false);
}

Processing::~Processing()
{

}

void Processing::set_stop_flag(bool flag)
{
	f_STOP.store(flag, std::memory_order_relaxed);
}

void Processing::set_scanning_buffer(Buffer<RADiCal_Scanning<float> >* p_Buffer)
{
	m_Scanning_Buffer = p_Buffer;
	m_SB_cv = p_Buffer->get_condition_variable();
}

bool Processing::scanning_buffer_empty()
{
	return m_Scanning_Buffer->empty();
}

void Processing::set_processing_buffer(Buffer<RADiCal_Detected_Peak<float> >* p_Buffer)
{
	m_Processing_Buffer = p_Buffer;
	m_PB_cv = p_Buffer->get_condition_variable();
}

bool Processing::processing_buffer_empty()
{
	return m_Processing_Buffer->empty();
}

void Processing::set_utility(Utility<float>* p_Utility)
{
	m_Utility = p_Utility;
}

void Processing::process()
{
	std::mutex mtx;
	std::unique_lock<std::mutex> lck (mtx, std::defer_lock);
	
	while(scanning_buffer_empty())
	{
		if(f_STOP.load(std::memory_order_relaxed) == true)
			return;
			
		m_SB_cv->wait(lck);
	}
		
	RADiCal_Scanning<float> r_Scanning = m_Scanning_Buffer->retrieve();
	Vector<std::complex <float> > Scanning_FFT = m_Utility->rad2_ct_fft(r_Scanning.s_Scanning, RADiCal_Number_Samples);
	Vector<float> Scanning_ABS_FFT = m_Utility->vector_abs(Scanning_FFT);
	std::vector<Detected_Peak<float> > v_Detected_Peaks = m_Utility->peak_det(Scanning_ABS_FFT);
	
	std::cerr << "Processing ha detectado " << (int)v_Detected_Peaks.size() << " picos" << std::endl;
	
	for(int i = 0; i < (int)v_Detected_Peaks.size(); ++i)
	{
		RADiCal_Detected_Peak<float> Peak = {r_Scanning.s_Pointing, v_Detected_Peaks[i]};
		m_Processing_Buffer->update(Peak);
	}
	
	std::cerr << "Processing ha procesado una medida" << std::endl;
}

void Processing::notify_SB()
{
	m_SB_cv->notify_all();
}

void Processing::notify_PB()
{
	m_SB_cv->notify_all();
}

void Processing::run()
{
	while(not f_STOP.load(std::memory_order_relaxed))
		process();
	
	std::cerr << "Processing se cierra" << std::endl;
}

void Processing_Task(Processing* p_Processing)
{
	p_Processing->run();
}
