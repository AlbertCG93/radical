#include "RADiCal_Const.h"

#include "ADC.h"

//Temporal includes
#include <iostream>
#include <thread>
#include <chrono>

ADC::ADC()
{
	open();
}

ADC::~ADC()
{
	close();
}

void ADC::open()
{
	
	short handle = ps2000_open_unit();
	
	if(handle == -1)
		std::cerr << "Error: Oscilloscope fails to open" << std::endl;
		
	if(handle == 0)
		std::cerr << "Error: No oscilloscope found" << std::endl;
	
	m_Handle = handle;
	
}

void ADC::close()
{
	
	short error_code = ps2000_stop(m_Handle);
	
	if(error_code == 0)
		std::cerr << "Error: Invalid handle passed" << std::endl;
	
}

void ADC::config()
{
	CHANNEL_CONFIG CH_A = {PS2000_CHANNEL_A, ADC_TRUE, ADC_FALSE, PS2000_5V};
	CHANNEL_CONFIG CH_B = {PS2000_CHANNEL_B, ADC_TRUE, ADC_FALSE, PS2000_5V};
	TRIGGER_CONFIG TRIGGER = {PS2000_CHANNEL_B, PS2000_MAX_VALUE/2, PS2000_RISING, 0, 0};
	
	//	1024 8 Fm = 78.125 kHz 
	MEASURE_CONFIG MEASURE = {RADiCal_Number_Presamples, RADiCal_Sampling_Frecuency_Code, RADiCal_Oversampling};
	
	config_oscilloscope(CH_A, CH_B, TRIGGER);
	config_measure(MEASURE);
}

void ADC::config_oscilloscope(CHANNEL_CONFIG CH_A, CHANNEL_CONFIG CH_B, TRIGGER_CONFIG TRIGGER)
{
	
	short error_code;
	
	///	We configure Channel A
	error_code = ps2000_set_channel(m_Handle, CH_A.CHANNEL, CH_A.ENABLED, CH_A.DC, CH_A.RANGE);
	if(error_code == 0)
		std::cerr << "Error: Couldn't set channel A" << std::endl;
		
	///	We configure Channel B
	error_code = ps2000_set_channel(m_Handle, CH_B.CHANNEL, CH_B.ENABLED, CH_B.DC, CH_B.RANGE);
	if(error_code == 0)
		std::cerr << "Error: Couldn't set channel B" << std::endl;
		
	switch(CH_A.RANGE)
	{
		case PS2000_50MV:
			m_Voltage_Range = 0.05;
			break;
		case PS2000_100MV:
			m_Voltage_Range = 0.1;
			break;
		case PS2000_200MV:
			m_Voltage_Range = 0.2;
			break;
		case PS2000_500MV:
			m_Voltage_Range = 0.5;
			break;
		case PS2000_1V:
			m_Voltage_Range = 1;
			break;
		case PS2000_2V:
			m_Voltage_Range = 2;
			break;
		case PS2000_5V:
			m_Voltage_Range = 5;
			break;
		case PS2000_10V:
			m_Voltage_Range = 10;
			break;
		case PS2000_20V:
			m_Voltage_Range = 20;
			break;
	}
		
	/// We configure Trigger
	error_code = ps2000_set_trigger(m_Handle, TRIGGER.SOURCE, TRIGGER.THRESHOLD, TRIGGER.DIRECTION, TRIGGER.DELAY, TRIGGER.AUTO_TRIGGER_MS);
	if(error_code == 0)
		std::cerr << "Error: Couldn't set trigger" << std::endl;
	
}

void ADC::config_measure(MEASURE_CONFIG MEASURE)
{
	
	m_Number_Samples = MEASURE.NUMBER_SAMPLES;
	
	///	We configure Time_Base
	m_Time_Base = MEASURE.TIME_BASE;
	
	m_Oversample = MEASURE.OVERSAMPLE;
	
}

Vector<float> ADC::get_measure()
{
	std::cerr << "-----ADC Getting measure-----" << std::endl;
	
	short error_code;
	int time_indisposed_ms;
	error_code = ps2000_run_block(m_Handle, m_Number_Samples, m_Time_Base, m_Oversample, &time_indisposed_ms);
	if(error_code == 0)
		std::cerr << "Error: Parameters out of range" << std::endl;
		
	std::cerr << "-----ADC Called run block-----" << std::endl;	
	
	std::this_thread::sleep_for(std::chrono::milliseconds(time_indisposed_ms));
	
	Vector<float> Scanning;
	
	std::chrono::steady_clock::time_point initial_time = std::chrono::steady_clock::now();
	while((std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::steady_clock::now() - initial_time)).count() < MAX_MEASURE_DELAY_SEC)
	{
		error_code = ps2000_ready(m_Handle);
		if(error_code == -1)
		{
			std::cerr << "Error: Oscilloscope not found" << std::endl;
			return Scanning;
		}
		if(error_code > 0)
		{
			short values[m_Number_Samples];
			short overflow;	
			
			short number_values = ps2000_get_values(m_Handle, values, NULL, NULL, NULL, &overflow, (long)m_Number_Samples);
			if(number_values == 0)
			{
				std::cerr << "Error: Parameters out of range" << std::endl;
				return Scanning;
			}
			if(number_values != m_Number_Samples)
			{
				std::cerr << "Error: Number_Values is different from Number_Samples" << std::endl;
				return Scanning;
			}
			
			Scanning = Vector<float>(m_Number_Samples);
			for(int i = 0; i < m_Number_Samples; ++i)
			{
				Scanning[i] = ((float)values[i])/((float)PS2000_MAX_VALUE)*m_Voltage_Range;
			}
				
			std::cerr << "-----ADC Returning measure-----" << std::endl;	
			
			return Scanning;
		}
	}
	
	std::cerr << "Error: Timeout on get_measure" << std::endl;
	
	return Scanning;
}
