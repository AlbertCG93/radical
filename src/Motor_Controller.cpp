#include "Motor_Controller.h"

#include <bitset>

#include <chrono>

#include <thread>

//temp include
#include <iostream>

//PortSettings settings = {BAUD28800, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
//PortSettings settings = {BAUD9600, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};

Motor_Controller::Motor_Controller()
{
	PortSettings settings = {BAUD9600, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
	
    motor_h.port = new QextSerialPort(PORT_H_NAME, settings, QextSerialPort::Polling);
    motor_v.port = new QextSerialPort(PORT_V_NAME, settings, QextSerialPort::Polling);
}

Motor_Controller::~Motor_Controller()
{
	close_ports();
}

void Motor_Controller::open_ports()
{
    if (!motor_h.port->isOpen()) 
    {
        motor_h.port->open(QIODevice::ReadWrite);
        std::cerr << "Motor_H ans: " << send_command(motor_h, "IS?").toStdString() << std::endl;
    }
    if (!motor_v.port->isOpen()) 
    {
        motor_v.port->open(QIODevice::ReadWrite);
        std::cerr << "Motor_V ans: " << send_command(motor_v, "IS?").toStdString() << std::endl;
    }
    
    std::cerr << "Motor ports opened" << std::endl;
}

void Motor_Controller::close_ports()
{
    if (motor_h.port->isOpen()) {
        motor_h.port->close();
    }
    if (motor_v.port->isOpen()) {
        motor_v.port->close();
    }
    
    std::cerr << "Motor ports closed" << std::endl;
}

void Motor_Controller::init_motors()
{
    reset_motor_initiators(motor_h);
    reset_motor_initiators(motor_v);
    
    send_command(motor_h, "PF" + SCAN_FAST);
    send_command(motor_v, "PF" + SCAN_FAST);
    
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    
    send_command(motor_h, "GI-");
    send_command(motor_v, "GI-");
    
    
    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        send_command(motor_h, "IS?");
        send_command(motor_v, "IS?");
    } while (motor_h.MOTOR_RUNS or motor_v.MOTOR_RUNS);
    
    std::cerr << "En inicio" << std::endl;
    
    //send_command(motor_h, "PF" + SCAN_SLOW);
    //send_command(motor_v, "PF" + SCAN_SLOW);
    
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    motor_h.pos = 0;
    motor_v.pos = 0;
    
    //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    
    reset_motor_initiators(motor_h);
    reset_motor_initiators(motor_v);
    
    std::cerr << "Motor ports initialized" << std::endl;
}

inline std::string Motor_Controller::create_frame(std::string data)
{
    std::string frame;

    std::string STX; STX += (char)2;
    std::string ETX; ETX += (char)3;
    std::string SEP (":");

    frame += STX;
    frame += ADDR;
    frame += data;
    frame += SEP;

    std::string CHKSUM = "XX";

    frame += CHKSUM;
    frame += ETX;

    return frame;
}

inline void Motor_Controller::reset_motor_initiators(Motor& m)
{
    send_command(m, "CI");
}

inline void Motor_Controller::set_motor_position(Motor& m, float deg, bool wait)
{
    if (m.LIM_SW_N or m.LIM_SW_P)
        reset_motor_initiators(m);
     
    if(m.pos == deg)
		return;
		
    int steps = deg*DEG2STEP;
    send_command(m, "GA" + QString::number(steps).toStdString());
    
    while (m.MOTOR_RUNS and wait) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(SL_INTERVAL_MS));
        
        send_command(m, "IS?");
    }
    
    m.pos = deg;
}

inline QString Motor_Controller::read_ans(Motor& m)
{
    return QString(m.port->readAll());
}

inline QString Motor_Controller::send_command(Motor& m, std::string cmd)
{
    m.port->write(create_frame(cmd).c_str());
    
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    
    QString ans = read_ans(m);
    update_motor_status(m, ans);
    
    return ans;
}

//Lee la respuesta al mandar una instruccion
// formato trama devuelta: STX ADDR SHORTSTATUS : DATA : CHKSUM ETX
// SHORTSTATUS son 2 bytes
inline void Motor_Controller::update_motor_status(Motor& m, QString ans)
{
    std::bitset<8> status_bits =  ans.remove(0,2).split(":").at(0).toInt();
    status_bits[6] ? m.ANY_ERROR = true : m.ANY_ERROR = false; //ANY ERROR
    status_bits[2] ? m.LIM_SW_N = true : m.LIM_SW_N = false; //LIMSW-
    status_bits[1] ? m.LIM_SW_P = true : m.LIM_SW_P = false; //LIMSW+
    status_bits[0] ? m.MOTOR_RUNS = true : m.MOTOR_RUNS = false; //MOTOR_RUNS
}

void Motor_Controller::set_limits(float limit_h, float limit_v)
{
	scan_limit_h = limit_h;
	scan_limit_v = limit_v;
}

void Motor_Controller::increment()
{
	float pos_h = motor_h.pos;
	float pos_v = motor_v.pos;
	
	if(pos_h == 0 and pos_v == 0)
		increment_step = BW;
	
	pos_v += increment_step;
	if(pos_v > scan_limit_v)
	{
		pos_h += BW;
		pos_v = scan_limit_v;
		
		increment_step *= -1;
	}
	if(pos_v < 0)
	{
		pos_h += BW;
		pos_v = 0;
		
		increment_step *= -1;
	}
	if(pos_h > scan_limit_h)
	{
		pos_h = 0;
		pos_v = 0;
	}
	
	set_motor_position(motor_h, pos_h, false);
	set_motor_position(motor_v, pos_v, false);
	
	while (motor_h.MOTOR_RUNS or motor_v.MOTOR_RUNS)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
        send_command(motor_h, "IS?");
        send_command(motor_v, "IS?");
    } 
}

Pointing<float> Motor_Controller::get_pointing()
{
	Pointing<float> pointing = {motor_h.pos, motor_v.pos};
	return pointing;
}

//en continu millor fer scan en horitzontal (menos parades)
//params: graus en vertical, graus horitzontal
/*
void Motor_Controller::start_scan(float scan_v, float scan_h)
{
    SCANNING = true;
    init_motors();
    int scan_counter = 0;
    do {
        for (int pos_h=0; pos_h<=scan_h; pos_h+=BW) {
            //QThread::msleep(100);
            for (int pos_v=0; pos_v<=scan_v; pos_v+=BW) {
                //QThread::msleep(100);
                if (!SCANNING)
                    break;
                if (pos_v%BW==0 && scan_counter%2==0)
                    set_motor_position(motor_v, pos_v, true);
                else
                    set_motor_position(motor_v, scan_v-pos_v, true);
            }

            if (!SCANNING)
                break;
            if (scan_counter%2==0)
                set_motor_position(motor_h, pos_h, true);
            else
                set_motor_position(motor_h, scan_h-pos_h, true);
        }
        scan_counter++;
    } while (SCANNING);

}
*/

void Motor_Controller::test()
{
	set_motor_position(motor_h, 10, false);
	set_motor_position(motor_v, 10, false);
	
	while (motor_h.MOTOR_RUNS or motor_v.MOTOR_RUNS)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        if(motor_h.MOTOR_RUNS)
			send_command(motor_h, "IS?");
		if(motor_v.MOTOR_RUNS)
			send_command(motor_v, "IS?");
    }
	
	std::cerr << "Test completed" << std::endl;
}
