#include "Display.h"

#include "RADiCal_Const.h"

Display::Display()
{
	 set_stop_flag(false);
}

Display::~Display()
{

}

void Display::set_stop_flag(bool flag)
{
	f_STOP.store(flag, std::memory_order_relaxed);
}

void Display::set_display_buffer(Buffer<RADiCal_Objective<float> >* p_Buffer)
{
	m_Display_Buffer = p_Buffer;
	m_DB_cv = p_Buffer->get_condition_variable();
}

bool Display::display_buffer_empty()
{
	return m_Display_Buffer->empty();
}

void Display::display()
{
	std::mutex mtx;
	std::unique_lock<std::mutex> lck (mtx, std::defer_lock);
	
	while(display_buffer_empty())
	{
		if(f_STOP.load(std::memory_order_relaxed) == true)
			return;
			
		m_DB_cv->wait(lck);
	}
		
	RADiCal_Objective<float> r_Objective = m_Display_Buffer->retrieve();
	
	std::cout << "Objective: " << std::endl;
	std::cout << "\t" << "Horitzontal distance: " << r_Objective.Horitzontal_Distance << std::endl;
	std::cout << std::endl;
}

void Display::notify_DB()
{
	m_DB_cv->notify_all();
}

void Display::run()
{
	while(not f_STOP.load(std::memory_order_relaxed))
		display();
	
	std::cerr << "Display se cierra" << std::endl;
}

void Display_Task(Display* p_Display)
{
	p_Display->run();
}
