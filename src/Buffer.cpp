#include "Buffer.h"

template <class T>
Buffer<T>::Buffer()
{

}

template <class T>
Buffer<T>::~Buffer()
{
    
}

template <class T>
std::condition_variable* Buffer<T>::get_condition_variable()
{
    return &m_cv;
}

template <class T>
bool Buffer<T>::empty()
{
    return m_Buffer.empty();
}

template <class T>
void Buffer<T>::update(T element)
{
    std::unique_lock<std::mutex> lck (m_mtx);
    
    m_Buffer.push(element);
    m_cv.notify_all();
}

template <class T>
T Buffer<T>::retrieve()
{
     std::unique_lock<std::mutex> lck (m_mtx);
     
     T element = m_Buffer.front(); m_Buffer.pop();
     
     return element; 
}

template class Buffer<RADiCal_Scanning<float> >;
template class Buffer<RADiCal_Detected_Peak<float> >;
template class Buffer<RADiCal_Objective<float> >;
