#include "Post_Processing.h"

#include "RADiCal_Const.h"

#include <cmath>

Post_Processing::Post_Processing()
{
	 set_stop_flag(false);
}

Post_Processing::~Post_Processing()
{

}

void Post_Processing::set_stop_flag(bool flag)
{
	f_STOP.store(flag, std::memory_order_relaxed);
}

void Post_Processing::set_processing_buffer(Buffer<RADiCal_Detected_Peak<float> >* p_Buffer)
{
	m_Processing_Buffer = p_Buffer;
	m_PB_cv = p_Buffer->get_condition_variable();
}

bool Post_Processing::processing_buffer_empty()
{
	return m_Processing_Buffer->empty();
}

void Post_Processing::set_display_buffer(Buffer<RADiCal_Objective<float> >* p_Buffer)
{
	m_Display_Buffer = p_Buffer;
	m_DB_cv = p_Buffer->get_condition_variable();
}

bool Post_Processing::display_buffer_empty()
{
	return m_Display_Buffer->empty();
}

void Post_Processing::process()
{
	std::mutex mtx;
	std::unique_lock<std::mutex> lck (mtx, std::defer_lock);
	
	while(processing_buffer_empty())
	{
		if(f_STOP.load(std::memory_order_relaxed) == true)
			return;
			
		m_PB_cv->wait(lck);
	}
		
	RADiCal_Detected_Peak<float> r_Peak = m_Processing_Buffer->retrieve();
	
	float amplitude = r_Peak.s_Detected_Peak.Amplitude;
	float frecuency = r_Peak.s_Detected_Peak.Frecuency;
	
	float radius = RADiCal_Tm_s * RADiCal_Light_Velocity/(4.0 * RADiCal_Delta_F_Hz) * frecuency;
	
	float horitzontal_distance = radius * cos(r_Peak.s_Pointing.Elevation);
	float height = radius * sin(r_Peak.s_Pointing.Elevation);
	
	RADiCal_Objective<float> r_Objective = {horitzontal_distance, r_Peak.s_Pointing.Azimuth, height, amplitude, false};
	
	m_Display_Buffer->update(r_Objective);

	std::cerr << "Post-Processing ha procesado una medida" << std::endl;
	
	std::cerr << "\t" << "Radius: " << radius << std::endl;
}

void Post_Processing::notify_PB()
{
	m_PB_cv->notify_all();
}

void Post_Processing::notify_DB()
{
	m_DB_cv->notify_all();
}

void Post_Processing::run()
{
	while(not f_STOP.load(std::memory_order_relaxed))
		process();
	
	std::cerr << "Post-Processing se cierra" << std::endl;
}

void Post_Processing_Task(Post_Processing* p_Post_Processing)
{
	p_Post_Processing->run();
}
