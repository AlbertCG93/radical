#include "Vector.h"

#include <complex>

template <class T>
Vector<T>::Vector()
{
	m_Vector = std::vector<T>();
}

template <class T>
Vector<T>::Vector(int dimension)
{
	m_Vector = std::vector<T>(dimension);
}

template <class T>
Vector<T>::~Vector()
{

}

template <class T>
inline Vector<T> operator+(Vector<T>& Vector_A, Vector<T>& Vector_B)
{
	if (Vector_A.dimension() != Vector_B.dimension())
		throw "Dimension mismatch";

	int n = Vector_A.dimension();
	
	Vector<T> Result = Vector<T>(n);
	for (int i = 0; i < n; ++i)
		Result[i] = Vector_A[i] + Vector_B[i];

	return Result;
}

template <class T>
inline Vector<T>& Vector<T>::operator+=(Vector<T>& Vector_B)
{
	*this = *this + Vector_B;

	return *this;
}

template <class T>
inline Vector<T> operator-(Vector<T>& Vector_A, Vector<T>& Vector_B)
{
	if (Vector_A.dimension() != Vector_B.dimension())
		throw "Dimension mismatch";

	int n = Vector_A.dimension();
	
	Vector<T> Result = Vector<T>(n);
	for (int i = 0; i < n; ++i)
		Result[i] = Vector_A[i] - Vector_B[i];

	return Result;
}

template <class T>
inline Vector<T>& Vector<T>::operator-=(Vector<T>& Vector_B)
{
	*this = *this - Vector_B;

	return *this;
}

template <class T>
inline Vector<T> operator*(T Scalar, Vector<T>& Vector_A)
{
	int n = Vector_A.dimension();
	
	Vector<T> Result = Vector<T>(n);
	for(int i = 0; i < n; ++i)
		Result[i] = Scalar * Vector_A[i];
		
	return Result;
}

template <class T>
inline T& Vector<T>::operator[](int index)
{
	return m_Vector[index];
}

template <class T>
inline std::ostream& operator<<(std::ostream& out, Vector<T> Vector_A)
{
	int n = Vector_A.dimension();

	for (int i = 0; i < n; ++i)
	{
		out << (i == 0 ? "" : " ") << Vector_A[i];
	}
	out << std::endl;

	return out;
}

template <class T>
void Vector<T>::push_back(T value)
{
	m_Vector.push_back(value);
}

template <class T>
inline int Vector<T>::dimension(void)
{
	return this->m_Vector.size();
}

// Explicit Instantiations
template class Vector<double>;
template std::ostream& operator<<(std::ostream&, Vector<double>);
template Vector<double> operator*(double, Vector<double>&);

template class Vector<std::complex<double> >;
template std::ostream& operator<<(std::ostream&, Vector<std::complex<double> >);
template Vector<std::complex<double> > operator*(std::complex<double>, Vector<std::complex<double> >&);

template class Vector<float>;
template std::ostream& operator<<(std::ostream&, Vector<float>);
template Vector<float> operator*(float, Vector<float>&);

template class Vector<std::complex<float> >;
template std::ostream& operator<<(std::ostream&, Vector<std::complex<float> >);
template Vector<std::complex<float> > operator*(std::complex<float>, Vector<std::complex<float> >&);


