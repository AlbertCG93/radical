#include "Main_Window.h"
#include "ui_Main_Window.h"
#include <QThread>
#include <iostream>
#include <chrono>

#include <string>
#include <QTimer>
#include <iostream>

QBrush normal_brush(Qt::lightGray);
QBrush is_target_brush(Qt::blue);

Main_Window::Main_Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Main_Window)
{
    ui->setupUi(this);
    prevIndex = 0;
    
    //create and configure polar plot
    polar_plot = new QwtPolarPlot(ui->widget);
    polar_plot->setMinimumWidth(400);
    polar_plot->setMinimumHeight(400);
    polar_plot->setPlotBackground(Qt::white);
    
    // rango de valores en los que trabaja el radar
    polar_plot->setScale(QwtPolar::Azimuth, 0, 360, 30);
    polar_plot->setScale(QwtPolar::Radius, 0, PLOT_RADIUS);
    
    // Crea la rejilla de valores (grid: axes, valores de azimuth...)
    d_grid = new QwtPolarGrid();
    d_grid->setPen( QPen( Qt::black) );
    
    //configurar rejilla (que aparezcan o no ciertos valores)
    d_grid->showAxis( QwtPolar::AxisAzimuth, true );
    d_grid->showAxis( QwtPolar::AxisLeft, false );
    d_grid->showAxis( QwtPolar::AxisRight, true );
    d_grid->showAxis( QwtPolar::AxisTop, true );
    d_grid->showAxis( QwtPolar::AxisBottom, false );
    d_grid->showGrid( QwtPolar::Azimuth, true );
    d_grid->setAxisPen(QwtPolar::AxisRight, QPen (Qt :: lightGray));
    d_grid->setAxisPen( QwtPolar::AxisTop, QPen (Qt :: lightGray) );
    d_grid->showGrid( QwtPolar::Radius, true);
    d_grid->attach( polar_plot );
    
    for (int sc = 0; sc <QwtPolar :: ScaleCount; sc ++)
    {
        d_grid -> showMinorGrid (sc);
        d_grid -> setMajorGridPen (sc, QPen (Qt :: lightGray));
        d_grid ->setMinorGridPen(sc, QPen ( Qt :: lightGray , 0 , Qt :: DotLine ));
    }
    setWindowTitle("RADiCal");
}

Main_Window::~Main_Window()
{
    delete ui;
}

void Main_Window::displayTarget(QString s)
{
	QStringList sl = s.split(":");
    RADiCal_Objective<float> r_Objective = {
		sl.at(0).toFloat(), sl.at(1).toFloat(), sl.at(2).toFloat(), 
		sl.at(3).toFloat(), sl.at(4).toInt()==1
	 };
    
    ui->targetList->addItem(
        new QListWidgetItem("Objective "+QString::number(ui->targetList->count()+1))
    );
    target t;
    t.ro = r_Objective;

    //create associated marker
    QwtPolarMarker* marker = new QwtPolarMarker();
    
    marker->setPosition(QwtPointPolar(t.ro.Azimuth, t.ro.Horitzontal_Distance));
    marker->setSymbol(
        new QwtSymbol(QwtSymbol::Ellipse,QBrush(Qt::lightGray), QPen(Qt::black), QSize(5,5))
    );
    
    marker->setLabelAlignment(Qt::AlignHCenter | Qt::AlignTop);
    
    QwtText text("Objective " + QString::number(ui->targetList->count()));
    text.setColor(Qt::black);
    
    QColor bg( Qt::white );
    bg.setAlpha( 200 );
    
    text.setBackgroundBrush( QBrush( bg ) );
    
    marker->setLabel( text );
    t.marker = marker;

    marker->attach(polar_plot);
    polar_plot->replot();

    targets.push_back(t);
}

void Main_Window::on_targetList_currentRowChanged(int currentRow)
{
    //previous
    QBrush brush = targets.at(prevIndex).ro.isTARGET ? is_target_brush : normal_brush;
    targets.at(prevIndex).marker->setSymbol(
        new QwtSymbol(QwtSymbol::Ellipse, brush, QPen(Qt::black), QSize(5,5))
    );
    //actual
    targets.at(currentRow).marker->setSymbol( new QwtSymbol(QwtSymbol::Ellipse,
        QBrush(Qt::darkRed), QPen(Qt::black), QSize(5,5))
    );
    RADiCal_Objective<float> ro = targets.at(currentRow).ro;
    ui->azimuthEdit->setText(QString::number(ro.Azimuth));
    ui->distanceEdit->setText(QString::number(ro.Horitzontal_Distance));
    ui->heightEdit->setText(QString::number(ro.Height));
    ui->radarcrossEdit->setText(QString::number(ro.RCS));
    polar_plot->replot();
    prevIndex = currentRow;    
}

void Main_Window::set_shutdown(void (*f)())
{
	shutdown = f;
}

void Main_Window::closeEvent(QCloseEvent *event)
{
    shutdown();
}

void Main_Window::send_message(std::string msg) {
    ui->textEdit->append(QString(msg.c_str()));
}

