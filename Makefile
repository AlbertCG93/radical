#############################################################################
# Makefile for building: bin/RADiCal
# Generated by qmake (2.01a) (Qt 4.8.6) on: jue dic 10 22:13:53 2015
# Project:  RADiCal.pro
# Template: app
# Command: /usr/lib/x86_64-linux-gnu/qt4/bin/qmake -o Makefile RADiCal.pro
#############################################################################

####### Compiler, tools and options

CC            = gcc
CXX           = g++
DEFINES       = -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED
CFLAGS        = -m64 -pipe -O2 -D_REENTRANT -Wall -W $(DEFINES)
CXXFLAGS      = -m64 -pipe -std=c++11 -O2 -D_REENTRANT -Wall -W $(DEFINES)
INCPATH       = -I/usr/share/qt4/mkspecs/linux-g++-64 -I. -I/usr/include/qt4/QtCore -I/usr/include/qt4/QtGui -I/usr/include/qt4 -I-I -Iinclude -I/../usr/local/qwtpolar-1.1.1/include -I/../usr/local/qwt-6.1.2/include -I. -I.
LINK          = g++
LFLAGS        = -m64 -Wl,-O1
LIBS          = $(SUBLIBS)  -L/usr/lib/x86_64-linux-gnu -L ./lib -lps2000 -Wl,--no-as-needed -pthread -L/home/albert/Documents/RADiCal/../../../../../usr/local/qwt-6.1.2/lib/ -lqwt -L/home/albert/Documents/RADiCal/../../../../../usr/local/qwtpolar-1.1.1/lib/ -lqwtpolar -lpthread -lQtGui -lQtCore 
AR            = ar cqs
RANLIB        = 
QMAKE         = /usr/lib/x86_64-linux-gnu/qt4/bin/qmake
TAR           = tar -cf
COMPRESS      = gzip -9f
COPY          = cp -f
SED           = sed
COPY_FILE     = $(COPY)
COPY_DIR      = $(COPY) -r
STRIP         = strip
INSTALL_FILE  = install -m 644 -p
INSTALL_DIR   = $(COPY_DIR)
INSTALL_PROGRAM = install -m 755 -p
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p

####### Output directory

OBJECTS_DIR   = ./

####### Files

SOURCES       = src/RADiCal.cpp \
		src/Display_Manager.cpp \
		src/Vector.cpp \
		src/Utility.cpp \
		src/Buffer.cpp \
		src/Motor_Controller.cpp \
		src/ADC.cpp \
		src/Acquisition.cpp \
		src/Processing.cpp \
		src/Post_Processing.cpp \
		src/Main_Window.cpp \
		src/qextserialport.cpp \
		src/qextserialport_unix.cpp moc_Display_Manager.cpp \
		moc_Main_Window.cpp
OBJECTS       = RADiCal.o \
		Display_Manager.o \
		Vector.o \
		Utility.o \
		Buffer.o \
		Motor_Controller.o \
		ADC.o \
		Acquisition.o \
		Processing.o \
		Post_Processing.o \
		Main_Window.o \
		qextserialport.o \
		qextserialport_unix.o \
		moc_Display_Manager.o \
		moc_Main_Window.o
DIST          = /usr/share/qt4/mkspecs/common/unix.conf \
		/usr/share/qt4/mkspecs/common/linux.conf \
		/usr/share/qt4/mkspecs/common/gcc-base.conf \
		/usr/share/qt4/mkspecs/common/gcc-base-unix.conf \
		/usr/share/qt4/mkspecs/common/g++-base.conf \
		/usr/share/qt4/mkspecs/common/g++-unix.conf \
		/usr/share/qt4/mkspecs/qconfig.pri \
		/usr/share/qt4/mkspecs/features/qt_functions.prf \
		/usr/share/qt4/mkspecs/features/qt_config.prf \
		/usr/share/qt4/mkspecs/features/exclusive_builds.prf \
		/usr/share/qt4/mkspecs/features/default_pre.prf \
		/usr/share/qt4/mkspecs/features/release.prf \
		/usr/share/qt4/mkspecs/features/default_post.prf \
		/usr/share/qt4/mkspecs/features/unix/thread.prf \
		/usr/share/qt4/mkspecs/features/shared.prf \
		/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf \
		/usr/share/qt4/mkspecs/features/warn_on.prf \
		/usr/share/qt4/mkspecs/features/qt.prf \
		/usr/share/qt4/mkspecs/features/moc.prf \
		/usr/share/qt4/mkspecs/features/resources.prf \
		/usr/share/qt4/mkspecs/features/uic.prf \
		/usr/share/qt4/mkspecs/features/yacc.prf \
		/usr/share/qt4/mkspecs/features/lex.prf \
		/usr/share/qt4/mkspecs/features/include_source_dir.prf \
		RADiCal.pro
QMAKE_TARGET  = RADiCal
DESTDIR       = bin/
TARGET        = bin/RADiCal

first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

all: Makefile $(TARGET)

$(TARGET): ui_Main_Window.h $(OBJECTS)  
	@$(CHK_DIR_EXISTS) bin/ || $(MKDIR) bin/ 
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

Makefile: RADiCal.pro  /usr/share/qt4/mkspecs/linux-g++-64/qmake.conf /usr/share/qt4/mkspecs/common/unix.conf \
		/usr/share/qt4/mkspecs/common/linux.conf \
		/usr/share/qt4/mkspecs/common/gcc-base.conf \
		/usr/share/qt4/mkspecs/common/gcc-base-unix.conf \
		/usr/share/qt4/mkspecs/common/g++-base.conf \
		/usr/share/qt4/mkspecs/common/g++-unix.conf \
		/usr/share/qt4/mkspecs/qconfig.pri \
		/usr/share/qt4/mkspecs/features/qt_functions.prf \
		/usr/share/qt4/mkspecs/features/qt_config.prf \
		/usr/share/qt4/mkspecs/features/exclusive_builds.prf \
		/usr/share/qt4/mkspecs/features/default_pre.prf \
		/usr/share/qt4/mkspecs/features/release.prf \
		/usr/share/qt4/mkspecs/features/default_post.prf \
		/usr/share/qt4/mkspecs/features/unix/thread.prf \
		/usr/share/qt4/mkspecs/features/shared.prf \
		/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf \
		/usr/share/qt4/mkspecs/features/warn_on.prf \
		/usr/share/qt4/mkspecs/features/qt.prf \
		/usr/share/qt4/mkspecs/features/moc.prf \
		/usr/share/qt4/mkspecs/features/resources.prf \
		/usr/share/qt4/mkspecs/features/uic.prf \
		/usr/share/qt4/mkspecs/features/yacc.prf \
		/usr/share/qt4/mkspecs/features/lex.prf \
		/usr/share/qt4/mkspecs/features/include_source_dir.prf \
		/usr/lib/x86_64-linux-gnu/libQtGui.prl \
		/usr/lib/x86_64-linux-gnu/libQtCore.prl
	$(QMAKE) -o Makefile RADiCal.pro
/usr/share/qt4/mkspecs/common/unix.conf:
/usr/share/qt4/mkspecs/common/linux.conf:
/usr/share/qt4/mkspecs/common/gcc-base.conf:
/usr/share/qt4/mkspecs/common/gcc-base-unix.conf:
/usr/share/qt4/mkspecs/common/g++-base.conf:
/usr/share/qt4/mkspecs/common/g++-unix.conf:
/usr/share/qt4/mkspecs/qconfig.pri:
/usr/share/qt4/mkspecs/features/qt_functions.prf:
/usr/share/qt4/mkspecs/features/qt_config.prf:
/usr/share/qt4/mkspecs/features/exclusive_builds.prf:
/usr/share/qt4/mkspecs/features/default_pre.prf:
/usr/share/qt4/mkspecs/features/release.prf:
/usr/share/qt4/mkspecs/features/default_post.prf:
/usr/share/qt4/mkspecs/features/unix/thread.prf:
/usr/share/qt4/mkspecs/features/shared.prf:
/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf:
/usr/share/qt4/mkspecs/features/warn_on.prf:
/usr/share/qt4/mkspecs/features/qt.prf:
/usr/share/qt4/mkspecs/features/moc.prf:
/usr/share/qt4/mkspecs/features/resources.prf:
/usr/share/qt4/mkspecs/features/uic.prf:
/usr/share/qt4/mkspecs/features/yacc.prf:
/usr/share/qt4/mkspecs/features/lex.prf:
/usr/share/qt4/mkspecs/features/include_source_dir.prf:
/usr/lib/x86_64-linux-gnu/libQtGui.prl:
/usr/lib/x86_64-linux-gnu/libQtCore.prl:
qmake:  FORCE
	@$(QMAKE) -o Makefile RADiCal.pro

dist: 
	@$(CHK_DIR_EXISTS) .tmp/RADiCal1.0.0 || $(MKDIR) .tmp/RADiCal1.0.0 
	$(COPY_FILE) --parents $(SOURCES) $(DIST) .tmp/RADiCal1.0.0/ && $(COPY_FILE) --parents include/RADiCal_Const.h include/Display_Manager.h include/Structures.h include/Vector.h include/Utility.h include/Buffer.h include/Motor_Controller.h include/ADC.h include/Acquisition.h include/Processing.h include/Post_Processing.h include/Main_Window.h include/qextserialport.h include/qextserialport_p.h include/qextserialport_global.h .tmp/RADiCal1.0.0/ && $(COPY_FILE) --parents src/RADiCal.cpp src/Display_Manager.cpp src/Vector.cpp src/Utility.cpp src/Buffer.cpp src/Motor_Controller.cpp src/ADC.cpp src/Acquisition.cpp src/Processing.cpp src/Post_Processing.cpp src/Main_Window.cpp src/qextserialport.cpp src/qextserialport_unix.cpp .tmp/RADiCal1.0.0/ && $(COPY_FILE) --parents include/Main_Window.ui .tmp/RADiCal1.0.0/ && (cd `dirname .tmp/RADiCal1.0.0` && $(TAR) RADiCal1.0.0.tar RADiCal1.0.0 && $(COMPRESS) RADiCal1.0.0.tar) && $(MOVE) `dirname .tmp/RADiCal1.0.0`/RADiCal1.0.0.tar.gz . && $(DEL_FILE) -r .tmp/RADiCal1.0.0


clean:compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) *~ core *.core


####### Sub-libraries

distclean: clean
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) Makefile


check: first

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

compiler_moc_header_make_all: moc_Display_Manager.cpp moc_Main_Window.cpp moc_qextserialport.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_Display_Manager.cpp moc_Main_Window.cpp moc_qextserialport.cpp
moc_Display_Manager.cpp: include/Structures.h \
		include/Vector.h \
		include/Buffer.h \
		include/Main_Window.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_plot.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_global.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_itemdict.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_item.h \
		/../usr/local/qwt-6.1.2/include/qwt_text.h \
		/usr/local/qwt-6.1.2/include/qwt_global.h \
		/../usr/local/qwt-6.1.2/include/qwt_legend_data.h \
		/../usr/local/qwt-6.1.2/include/qwt_graphic.h \
		/usr/local/qwt-6.1.2/include/qwt_null_paintdevice.h \
		/../usr/local/qwt-6.1.2/include/qwt_interval.h \
		/../usr/local/qwt-6.1.2/include/qwt_scale_map.h \
		/usr/local/qwt-6.1.2/include/qwt_transform.h \
		/../usr/local/qwt-6.1.2/include/qwt_point_polar.h \
		/usr/local/qwt-6.1.2/include/qwt_math.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_grid.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_marker.h \
		/../usr/local/qwt-6.1.2/include/qwt_symbol.h \
		include/Display_Manager.h
	/usr/lib/x86_64-linux-gnu/qt4/bin/moc $(DEFINES) $(INCPATH) include/Display_Manager.h -o moc_Display_Manager.cpp

moc_Main_Window.cpp: include/Structures.h \
		include/Vector.h \
		include/Buffer.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_plot.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_global.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_itemdict.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_item.h \
		/../usr/local/qwt-6.1.2/include/qwt_text.h \
		/usr/local/qwt-6.1.2/include/qwt_global.h \
		/../usr/local/qwt-6.1.2/include/qwt_legend_data.h \
		/../usr/local/qwt-6.1.2/include/qwt_graphic.h \
		/usr/local/qwt-6.1.2/include/qwt_null_paintdevice.h \
		/../usr/local/qwt-6.1.2/include/qwt_interval.h \
		/../usr/local/qwt-6.1.2/include/qwt_scale_map.h \
		/usr/local/qwt-6.1.2/include/qwt_transform.h \
		/../usr/local/qwt-6.1.2/include/qwt_point_polar.h \
		/usr/local/qwt-6.1.2/include/qwt_math.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_grid.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_marker.h \
		/../usr/local/qwt-6.1.2/include/qwt_symbol.h \
		include/Main_Window.h
	/usr/lib/x86_64-linux-gnu/qt4/bin/moc $(DEFINES) $(INCPATH) include/Main_Window.h -o moc_Main_Window.cpp

moc_qextserialport.cpp: include/qextserialport_global.h \
		include/qextserialport.h
	/usr/lib/x86_64-linux-gnu/qt4/bin/moc $(DEFINES) $(INCPATH) include/qextserialport.h -o moc_qextserialport.cpp

compiler_rcc_make_all:
compiler_rcc_clean:
compiler_image_collection_make_all: qmake_image_collection.cpp
compiler_image_collection_clean:
	-$(DEL_FILE) qmake_image_collection.cpp
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all: ui_Main_Window.h
compiler_uic_clean:
	-$(DEL_FILE) ui_Main_Window.h
ui_Main_Window.h: include/Main_Window.ui
	/usr/lib/x86_64-linux-gnu/qt4/bin/uic include/Main_Window.ui -o ui_Main_Window.h

compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_moc_header_clean compiler_uic_clean 

####### Compile

RADiCal.o: src/RADiCal.cpp include/RADiCal_Const.h \
		include/Structures.h \
		include/Vector.h \
		include/Buffer.h \
		include/Utility.h \
		include/Motor_Controller.h \
		include/qextserialport.h \
		include/qextserialport_global.h \
		include/ADC.h \
		include/ps2000.h \
		include/Acquisition.h \
		include/Processing.h \
		include/Post_Processing.h \
		include/Display_Manager.h \
		include/Main_Window.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_plot.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_global.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_itemdict.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_item.h \
		/../usr/local/qwt-6.1.2/include/qwt_text.h \
		/usr/local/qwt-6.1.2/include/qwt_global.h \
		/../usr/local/qwt-6.1.2/include/qwt_legend_data.h \
		/../usr/local/qwt-6.1.2/include/qwt_graphic.h \
		/usr/local/qwt-6.1.2/include/qwt_null_paintdevice.h \
		/../usr/local/qwt-6.1.2/include/qwt_interval.h \
		/../usr/local/qwt-6.1.2/include/qwt_scale_map.h \
		/usr/local/qwt-6.1.2/include/qwt_transform.h \
		/../usr/local/qwt-6.1.2/include/qwt_point_polar.h \
		/usr/local/qwt-6.1.2/include/qwt_math.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_grid.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_marker.h \
		/../usr/local/qwt-6.1.2/include/qwt_symbol.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o RADiCal.o src/RADiCal.cpp

Display_Manager.o: src/Display_Manager.cpp include/Display_Manager.h \
		include/Structures.h \
		include/Vector.h \
		include/Buffer.h \
		include/Main_Window.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_plot.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_global.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_itemdict.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_item.h \
		/../usr/local/qwt-6.1.2/include/qwt_text.h \
		/usr/local/qwt-6.1.2/include/qwt_global.h \
		/../usr/local/qwt-6.1.2/include/qwt_legend_data.h \
		/../usr/local/qwt-6.1.2/include/qwt_graphic.h \
		/usr/local/qwt-6.1.2/include/qwt_null_paintdevice.h \
		/../usr/local/qwt-6.1.2/include/qwt_interval.h \
		/../usr/local/qwt-6.1.2/include/qwt_scale_map.h \
		/usr/local/qwt-6.1.2/include/qwt_transform.h \
		/../usr/local/qwt-6.1.2/include/qwt_point_polar.h \
		/usr/local/qwt-6.1.2/include/qwt_math.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_grid.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_marker.h \
		/../usr/local/qwt-6.1.2/include/qwt_symbol.h \
		include/RADiCal_Const.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Display_Manager.o src/Display_Manager.cpp

Vector.o: src/Vector.cpp include/Vector.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Vector.o src/Vector.cpp

Utility.o: src/Utility.cpp include/RADiCal_Const.h \
		include/Utility.h \
		include/Structures.h \
		include/Vector.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Utility.o src/Utility.cpp

Buffer.o: src/Buffer.cpp include/Buffer.h \
		include/Structures.h \
		include/Vector.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Buffer.o src/Buffer.cpp

Motor_Controller.o: src/Motor_Controller.cpp include/Motor_Controller.h \
		include/Structures.h \
		include/Vector.h \
		include/qextserialport.h \
		include/qextserialport_global.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Motor_Controller.o src/Motor_Controller.cpp

ADC.o: src/ADC.cpp include/RADiCal_Const.h \
		include/ADC.h \
		include/ps2000.h \
		include/Vector.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ADC.o src/ADC.cpp

Acquisition.o: src/Acquisition.cpp include/Acquisition.h \
		include/Structures.h \
		include/Vector.h \
		include/Motor_Controller.h \
		include/qextserialport.h \
		include/qextserialport_global.h \
		include/ADC.h \
		include/ps2000.h \
		include/Buffer.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Acquisition.o src/Acquisition.cpp

Processing.o: src/Processing.cpp include/RADiCal_Const.h \
		include/Processing.h \
		include/Structures.h \
		include/Vector.h \
		include/Buffer.h \
		include/Utility.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Processing.o src/Processing.cpp

Post_Processing.o: src/Post_Processing.cpp include/Post_Processing.h \
		include/Structures.h \
		include/Vector.h \
		include/Buffer.h \
		include/RADiCal_Const.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Post_Processing.o src/Post_Processing.cpp

Main_Window.o: src/Main_Window.cpp include/Main_Window.h \
		include/Structures.h \
		include/Vector.h \
		include/Buffer.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_plot.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_global.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_itemdict.h \
		/usr/local/qwtpolar-1.1.1/include/qwt_polar_item.h \
		/../usr/local/qwt-6.1.2/include/qwt_text.h \
		/usr/local/qwt-6.1.2/include/qwt_global.h \
		/../usr/local/qwt-6.1.2/include/qwt_legend_data.h \
		/../usr/local/qwt-6.1.2/include/qwt_graphic.h \
		/usr/local/qwt-6.1.2/include/qwt_null_paintdevice.h \
		/../usr/local/qwt-6.1.2/include/qwt_interval.h \
		/../usr/local/qwt-6.1.2/include/qwt_scale_map.h \
		/usr/local/qwt-6.1.2/include/qwt_transform.h \
		/../usr/local/qwt-6.1.2/include/qwt_point_polar.h \
		/usr/local/qwt-6.1.2/include/qwt_math.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_grid.h \
		/../usr/local/qwtpolar-1.1.1/include/qwt_polar_marker.h \
		/../usr/local/qwt-6.1.2/include/qwt_symbol.h \
		include/ui_Main_Window.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Main_Window.o src/Main_Window.cpp

qextserialport.o: src/qextserialport.cpp include/qextserialport.h \
		include/qextserialport_global.h \
		include/qextserialport_p.h \
		moc_qextserialport.cpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o qextserialport.o src/qextserialport.cpp

qextserialport_unix.o: src/qextserialport_unix.cpp include/qextserialport.h \
		include/qextserialport_global.h \
		include/qextserialport_p.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o qextserialport_unix.o src/qextserialport_unix.cpp

moc_Display_Manager.o: moc_Display_Manager.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_Display_Manager.o moc_Display_Manager.cpp

moc_Main_Window.o: moc_Main_Window.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_Main_Window.o moc_Main_Window.cpp

####### Install

install:   FORCE

uninstall:   FORCE

FORCE:

