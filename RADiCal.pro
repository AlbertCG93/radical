#-------------------------------------------------
#
# Project created by QtCreator 2015-11-14T23:11:28
#
#-------------------------------------------------

QT       += core gui

CONFIG += qwt 
CONFIG += thread

RC_FILE = RADiCal_GUI.rc

QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ./bin/RADiCal
TEMPLATE = app

DEPENDPATH += ./src

SOURCES += 	RADiCal.cpp				\
			Display_Manager.cpp		\
			Vector.cpp				\
			Utility.cpp				\
			Buffer.cpp				\
			Motor_Controller.cpp	\
			ADC.cpp					\
			Acquisition.cpp			\
			Processing.cpp			\
			Post_Processing.cpp		\
			Main_Window.cpp			\
			qextserialport.cpp		\
			qextserialport_unix.cpp	


INCLUDEPATH += -I ./include
DEPENDPATH += ./include

HEADERS  += RADiCal_Const.h			\
			Display_Manager.h		\
			Structures.h			\
			Vector.h				\
			Utility.h				\
			Buffer.h				\
			Motor_Controller.h		\
			ADC.h					\
			Acquisition.h			\
			Processing.h			\
			Post_Processing.h		\
			Main_Window.h			\
			qextserialport.h		\
			qextserialport_p.h		\
			qextserialport_global.h	\
			

FORMS    += Main_Window.ui

LIBS += -L ./lib -lps2000
LIBS += -Wl,--no-as-needed -pthread

LIBS += -L$$PWD/../../../../../usr/local/qwt-6.1.2/lib/ -lqwt
LIBS += -L$$PWD/../../../../../usr/local/qwtpolar-1.1.1/lib/ -lqwtpolar

INCLUDEPATH += $$PWD/../../../../../usr/local/qwtpolar-1.1.1/include
INCLUDEPATH += $$PWD/../../../../../usr/local/qwt-6.1.2/include

DEPENDPATH += $$PWD/../../../../../usr/local/qwtpolar-1.1.1/include
DEPENDPATH += $$PWD/../../../../../usr/local/qwt-6.1.2/include
